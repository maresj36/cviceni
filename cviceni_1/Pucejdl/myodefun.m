function f = myodefun(t,x,ut,u, A, B)
% auxiliary function for solving ODE with time-varying input.

u = interp1(ut,u,t); % interpolate input vector in case it's sampling does not match the simulation
f = (A*x + B*u); % construct f at time t for ODE solver
end