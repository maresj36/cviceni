clc;
%% Priklad 1: Matematicke kyvadlo, odvozeni nelinearnich rovnic
% Moment setrvacnosti:
% 	- https://cs.wikipedia.org/wiki/Moment_setrva%C4%8Dnosti
% 	- J = m*r^2

%% Simulovani nelinearniho modelu pomoci ode45
%   https://www.mathworks.com/help/matlab/ref/ode45.html

g = 9.81;                               % m.s-1 graviracni zrychleni
L = 1;                                  % m delka kyvadla
f = @(t,x) [x(2);-g/L*sin(x(1))];       % definice funkce
x0 = [pi/2; 0];                          % Pocatecni podminky: [poloha, rychlost]
tspan = [0 10];                         % Cas: [zacatek, konec]
[T,Y] = ode45(f,tspan,x0);

plot(T,Y(:,1),'-',T,Y(:,2),'-.');
xlabel('time [s]');
legend('poloha','rychlost');
grid on;




%% Linearni stavovy model
%   Linearizace:
%       - Video:
%       	cas: 8:50
%       	https://youtu.be/JZEfmNztV80?t=530
%       - Ucebni text z ARI:
%       	str: 13
%       	http://www.polyx.cz/_ari/slajdy/lin3.pdf
g = 9.81;                               % m.s-1 graviracni zrychleni
L = 1;                                  % m delka kyvadla
A = [0,1;-g/L,0]; B=[0;0];
C = [1,0]; D=0;
sys1 = ss(A,B,C,D,...
    'statename',{'poloha' 'rychlost'},...
    'outputname','poloha');
figure;
x0 = [0 0.1];                           % pocatecni podminky
initial(sys1,x0);

% figure;
% step(sys1);

%% Porovanani nelinearniho a linearniho modelu (odezva na pocatecni podminky)
x0 = [pi/2 0];                           % pocatecni podminky [uhel, uhlova rychlost]
tint = [0 10];
g = 9.81;                               % m.s-1 graviracni zrychleni
L = 1;                                  % m delka kyvadla

f = @(t,x) [x(2);-g/L*sin(x(1))];       % definice funkce
flin = @(t,x) [x(2);-g/L*x(1)];
[T1,Y1] = ode45(f,tint,x0);
[T2,Y2] = ode45(flin,tint,x0);
h = plot(T1,Y1(:,1),'-',T2,Y2(:,1),'-.');
legend('nelinearni','linearni');

%%
u = @(t) sin(t);

g = 9.81;                               % m.s-1 graviracni zrychleni
L = 1;                                  % m delka kyvadla
f = @(t,x) [x(2);-g/L*sin(x(1)) + 0*u(t)];       % definice funkce
x0 = [pi/2; 0];                          % Pocatecni podminky: [poloha, rychlost]
tspan = [0 10];                         % Cas: [zacatek, konec]
[T,Y] = ode45(f,tspan,x0);

plot(T,Y(:,1),'-',T,Y(:,2),'-.');
xlabel('time [s]');
legend('poloha','rychlost');
grid on;



