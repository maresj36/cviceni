%% Jednoduchy elektricky obvod popsany pomoci DAE
clc;
clear;
%% Parametry
R = 1;
Cap = 0.250;
%% Matice DAE
A = zeros(6,6); % Nulova matice 6x6
A(1,2) = 1;
A(2,5) = 1;
A(3,4) = -1;
A(3,6) = 1;
A(4,3) = 1;
A(4,4) = -R;
A(5,2) = -1;
A(5,4) = 1;
A(6,1) = 1;
A(6,3) = 1;
A(6,5) = -1;

B = zeros(6,1);
B(2,1) = -1;

C = zeros(1,6);
C(1,1) = 1; % Vystup zvoleno napeti na kondenzatoru C1

E = zeros(6,6);
E(1,1) = Cap;

% D = zeros(1,6);

%% Konstantni vstup
u = 1; % Volba konstantniho vstupu

% ode15s: https://www.mathworks.com/help/matlab/ref/ode15s.html#bu5yps7_sep_shared-odefun 
f = @(t,x) (A*x + B*u);
options = odeset('Mass',E);     % 
tspan = [0 5];          % Casove okno, v kterem se resi funkce f
x0 = zeros(1,6);        % Pocatecni podminky
[T,Y] = ode15s(f,tspan, x0, options);

% Plot
figure;
plot(T,Y);
legend('u_{C1}','i_{C1}','u_{R1}','i_{R1}','u_U','i_U');



%% Promenny vstup
N = 1000; % Pocet kroku
tspan = linspace(0,5,N);
u = sin(tspan);
% u = [5*ones(1,N/2) 10*ones(1,N/2)];
options = odeset('Mass',E);     % 
x0 = zeros(1,6);
output = zeros(N,6);


%% Pouziti pomocne funkce
[time,output] = ode15s(@(t,x) myodefun(t,x,tspan,u, A, B), tspan, x0, options);

%% Pouziti for-cyklu
for i=2:N
    f = @(t,x) (A*x + B*u(i));
    curr_tspan = [tspan(i-1) tspan(i)];
    [T,Y] = ode15s(f,curr_tspan, x0, options);
    
    % Save output
    output(i,:) = Y(end,:);
    
    % Next iteration
    x0 = Y(end,:);
end
%%
% Plot
figure;
plot(tspan,output);
legend('u_{C1}','i_{C1}','u_{R1}','i_{R1}','u_U','i_U');

%% Explicitni realizace DAE (prevod na stavove rovnice):
% POZOR: Timto zpusobem se nemusi zachovat fyzikalni struktura systemu (stavy neodpovidaji napeti na kondenzatorech / proudum na civkach)
sysd = dss(A,B,C,0,E); % Descriptor state-space: https://www.mathworks.com/help/control/ref/dss.html
ss_realization = ss(sysd, 'explicit');
figure;
step(ss_realization);


%% Prevod na stavove rovnice se zachováním fyzikální struktury
A11 = E(1,1)\A(1,1);
A12 = E(1,1)\A(1,2:end);
A21 = A(2:end,1);
A22 = A(2:end, 2:end);
rank(A22) %Test, zda je matice regularni

B1 = E(1,1)\B(1,1);
B2 = B(2:end,1);

A_new = (A11 - A12/A22*A21);
B_new = (-A12/A22*B2 + B1);

ss_new = ss(A_new,B_new,1,0);
figure;
step(ss_new);



