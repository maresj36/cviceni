clc;
clear;
%% Four rooms
%% Parameters
% nodes: distinct temperatures
% [1 ... 4]: room temperatures
% [5 ... 12]: inner wall temperatures
% [13 ... 20]: outer wall temperatures
% [21]: Outside temperature

nodes = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21];
num_nodes = numel(nodes);


Adj = zeros(num_nodes);
Adj(1,5) = 1;
Adj(1,9) = 1;
Adj(1,13) = 1;
Adj(2,6) = 1;
Adj(2,7) = 1;
Adj(2,15) = 1;
Adj(3,10) = 1;
Adj(3,11) = 1;
Adj(3,17) = 1;
Adj(4,8) = 1;
Adj(4,12) = 1;
Adj(4,19) = 1;
Adj(5,6) = 1;
Adj(7,8) = 1;
Adj(9,10) = 1;
Adj(11,12) = 1;
Adj(13,14) = 1;
Adj(14,21) = 1;
Adj(15,16) = 1;
Adj(16,21) = 1;
Adj(17,18) = 1;
Adj(18,21) = 1;
Adj(19,20) = 1;
Adj(20,21) = 1;

% Create bidirectional adjacency matrix
Adj = Adj+ Adj'; %Watch out - if there are ones on the diagonal (self adjacency
                 %it would be necessary to subtract them once

%% Thermal Capacitances
Cap = zeros(1,num_nodes);

% Thermal capacitance of rooms (order e7 is reasonable)
Cap(1) = 7.2e7;
Cap(2) = 7.2e7;
Cap(3) = 7.2e7;
Cap(4) = 7.2e7;

% Thermal capacitance of 'outside' (order e50 is reasonable)
Cap(21) = 1e50;   % High capacitance effectively stands for hard temperature input

% NOTE: Not a particularly nice way of doing this. Basically creates a stiff
% problem by inducing ultra slow dynamics of the outside temperature.

% Inner walls of room 1
C1 = 1e5;
Cap(5) = C1;
Cap(9) = C1;
Cap(13) = C1;

% Inner walls of room 2
C2 = 1e5;
Cap(6) = C2;
Cap(7) = C2;
Cap(15) = C2;

% Inner walls of room 3
C3 = 1e5;
Cap(10) = C3;
Cap(11) = C3;
Cap(17) = C3;

% Inner walls of room 4
C4 = 1e5;
Cap(8) = C4;
Cap(12) = C4;
Cap(19) = C4;

% Outer walls of room 1
Cap(14) = 2e5;

% Outer walls of room 2
Cap(16) = 2e5;

% Outer walls of room 3
Cap(18) = 2e5;

% Outer walls of room 4
Cap(20) = 2e5;

%% Thermal Resistances
Res = zeros(num_nodes);

% Inner walls of room 1
R1 = 1/715;
Res(1,5) = R1;
Res(1,9) = R1;
Res(1,13) = R1;

% Inner walls of room 2
R2 = 1/715;
Res(2,6) = R2;
Res(2,7) = R2;
Res(2,15) = R2;

% Inner walls of room 3
R3 = 1/715;
Res(3,10) = R3;
Res(3,11) = R3;
Res(3,17) = R3;

% Inner walls of room 4
R4 = 1/715;
Res(4,8) = R4;
Res(4,12) = R4;
Res(4,19) = R4;

% Mid walls
R_mid = 0.5/1758;
Res(5,6) = R_mid;
Res(7,8) = R_mid;
Res(9,10) = R_mid;
Res(11,12) = R_mid;
Res(13,14) = R_mid;
Res(15,16) = R_mid;
Res(17,18) = R_mid;
Res(19,20) = R_mid;

% Outer walls of room 1
Res(14,21) = 0.5/715;

% Outer walls of room 2
Res(16,21) = 0.5/715;

% Outer walls of room 3
Res(18,21) = 0.5/715;

% Outer walls of room 4
Res(20,21) = 0.5/715;

Res = Res + Res';

%% System matrix, could be created more efficiently

A = zeros(num_nodes);

% Off-diagonal terms in A
for ii=1:num_nodes
    for jj=1:num_nodes
        if(ii ~= jj && Adj(ii,jj) == 1)
            A(ii,jj) = 1/(Cap(ii)*Res(ii,jj));
        end
    end
end

% Diagonal terms in A
for ii=1:num_nodes
    for jj=1:num_nodes
        if(ii == jj)
            A(ii,jj) = -sum(A(ii,:),2);
        end
    end
end

%% State space
B = zeros(num_nodes,1); % Input matrix
B(1) = 0.0004;            % Add heat flux input to room 1
C = zeros(4,21);    % Output matrix

% Observe only room temperatures
C(1,1) = 1;
C(2,2) = 1;
C(3,3) = 1;
C(4,4) = 1;


D = 0;
room4_sys = ss(A,B,C,D);

% initial temperatures

T0 = 10*ones(num_nodes,1);      % Initial temperature of walls

% Initial temperature of rooms, temperatures T1, T2, T3 and T4
T0(1) = 30;
T0(2) = 20;
T0(3) = 25;
T0(4) = 15;

% Outside temperature
Tout = -10;
T0(21) = Tout;


%% PLOT 3D visualization

% Run the simulation at 100 samples for some number of hours
hrs = 100;
t = linspace(0,hrs*3600,100);   % Simulate for 'hrs' hours

y = lsim(room4_sys,ones(size(t)),t,T0);

X = [1*ones(4,1), 2*ones(4,1)-eps, 2*ones(4,1)+eps, 3*ones(4,1)];
Y = X';


figure(1);

% Map the room layout to what we did in the example
a = 3;
b = 4;
c = 1;
d = 2;
% change default colormap for nicer surface plot
colormap(jet)

for ii=1:numel(t)
    Z = [y(ii,a), y(ii,a), y(ii,b), y(ii,b);
        y(ii,a), y(ii,a), y(ii,b), y(ii,b);
        y(ii,c), y(ii,c), y(ii,d), y(ii,d);
        y(ii,c), y(ii,c), y(ii,d), y(ii,d)];
    ax1 = surf(X,Y,Z);
    
    txt(1) = text(1.5,1.5,y(ii,a)+1.5,'\Theta_3','FontWeight', 'bold');
    txt(2) = text(2.5,1.5,y(ii,b)+1.5,'\Theta_4','FontWeight', 'bold');
    txt(3) = text(1.5,2.5,y(ii,c)+1.5,'\Theta_1','FontWeight', 'bold');
    txt(4) = text(2.5,2.5,y(ii,d)+1.5,'\Theta_2','FontWeight', 'bold');
    zlim([min(T0) max(T0)+15]);
    caxis([min(T0) max(T0)]);
    set(gca,'xticklabel',{[]})
    set(gca,'yticklabel',{[]})
    zlabel('Room temperature [^{\circ}C]')
    colorbar;
    %     view(0,90);
    title(['Time: ',num2str(round(t(ii)/3600)),' hours, outside temperature ',num2str(Tout),' ^{\circ}C'])
    drawnow;
end

