clc;
clear;
%% Heat equation
alpha = 1;  % Constant of the heat equation
L = 100;    % Length od domain
N = 1000;   % Number of disrete points both in space and in frequency 
dx = L/N;

x = -L/2:dx:L/2-dx;
tspan = 0:0.1:20;


%% Time domain
u = zeros(numel(tspan), N);    % Temperature in time domain
% Set initial temperature
u(1, 3/8*N:5/8*N) = 1;
figure; 
plot(x,u(1,:)); 
xlabel('Space x'); 
ylabel('Temperature');
ylim([-0.1, 1.5]);

%% Frequency domain
% Define spacial frequency
kappa_temp = (2*pi/L)*(-N/2:N/2-1);   
kappa = fftshift(kappa_temp);   % Reordered just because of how fft function is implemented in Matlab
figure; 
plot(kappa_temp); 
hold on; 
plot(kappa); 
legend('Kappa', 'Kappa reordered');

% Fourier transform of initial conditions
u0_hat = fft(u(1,:));   

% Define and Solve ODE
heat_rhs = @(t,y) -alpha^2*(kappa.^2)'.*y;
[~,u_hat] = ode45(heat_rhs,tspan, u0_hat);  

% Inverse Fourier transform
for ii = 1:numel(tspan)
    u(ii,:) = ifft(u_hat(ii,:));
end

%%
figure;
h = waterfall(x, tspan, u);
xlabel('Space x');
ylabel('Time t');
zlabel('Temperature u(x,t)');
%%
% figure;









