clc;
clear;
%% Definice DC motoru:
R = 2.0;                % Ohm
L = 0.5;                % Henry
K = 0.1;                % "Torque/back emf" konstanta 
J = 0.02;               % kg.m^2/s^2
b = 0.01;

A = [-b/J K/J; -K/L -R/L];
B = [0 ;1/L];
C = eye(2);

u = 1;
dc_motor_sys = @(t,x) A*x + B*u;

tspan = [0 1];
x0 = [0;0];
% [T,Y] = ode45(dc_motor_sys, tspan, x0);

% plot(T,Y);

