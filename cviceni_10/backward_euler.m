function [t, x] = backward_euler(F,tspan, x0, h)
%forward_euler First order method for numerical simulation
% F: Function to be solved
% tspan: [start_time, end_time]
% x0: initial conditions: column vector
% h: integration step
% 
% t: column vector: times of integration
% x: approximate solution of the F at times time_out

t0 = tspan(1); 
t_f = tspan(2); % Final time

% Preallocation of the memory
t = t0:h:t_f+h;                     % Define time_out vector
x = zeros(numel(t), numel(x0));  % Row: integration step, Column: values of the states
x(1,:) = x0';

% Initialization
k = 1;
options = optimoptions('fsolve','Display','off');
while t(k) < t_f
    func = @(x_kp1) x_kp1 - x(k,:)' - h*F(t(k+1),x_kp1);   % Non-linear function to be solved for x_kp1 <=> x_{k+1}
    start_p = x(k,:)';    % starting point of the solution, will be probably close to x(k), but can be chosen for instance [0;0];
    x(k+1,:) = fsolve(func,start_p, options);

    % Next iteration:
    k = k + 1;
end

